// https://docs.rs/dialoguer/0.8.0/dialoguer/
// https://docs.rs/dialoguer/0.8.0/dialoguer/struct.Confirm.html
// https://docs.rs/dialoguer/0.8.0/dialoguer/struct.Input.html
use dialoguer::{
    Confirm,
    Input,
    theme::ColorfulTheme
};
use std::thread::sleep;
use std::time::Duration;
use serde::{Deserialize, Serialize};
use serde_json::Result;

use std::fs::File;
use std::io::{self, BufReader, BufWriter};
use std::path::Path;


use crate::Node::*;

#[derive(Serialize, Deserialize, Clone)]
struct BinaryQ {
    question: String,
    yes: Box<Node>,
    no: Box<Node>,
}
#[derive(Serialize, Deserialize, Clone)]
enum Node {
    Question(BinaryQ),
    Item(String),
}

#[derive(Serialize, Deserialize, Clone)]
struct FileStructureForDB {
    domain: String,
    db: Node,
}

fn main() -> io::Result<()> {
    let mut domain = String::new();
    let mut db = Node::Item("Not Yet Initialized".into());

    read_or_initialize(&mut domain, &mut db)?;

    loop {
        println!("\nThink of anything in {}.", domain);
        println!("I will discover what you are thinking about in 20 questions or less.");
        sleep(Duration::from_millis(1000));
        println!("\n- - - - - - - - - - - - - - - - - - - - - - -\n");

        let mut this_node = &mut db;
        loop {
            match this_node {
                Question(ref mut bin_q) => {
                    this_node = if Confirm::with_theme(&ColorfulTheme::default())
                    .with_prompt(&bin_q.question)
                    .interact()? {
                        bin_q.yes.as_mut()
                    } else {
                        bin_q.no.as_mut()
                    };
                },
                Item(ref guess) => {
                    if Confirm::with_theme(&ColorfulTheme::default())
                    .with_prompt(format!("Are you thinking of {}?", &guess))
                    .interact()? {
                        println!("\nI'm so smart! 😉");
                        sleep(Duration::from_millis(1000));
                        break;
                    } else {
                        println!("\nDamn! 😒");
                        sleep(Duration::from_millis(1000));
                        let new_item = Input::new()
                        .with_prompt("\nWhat were you thinking of?")
                        .interact()?;
                        sleep(Duration::from_millis(200));
                        let prompt = format!("And what question could I ask to distinguish between {} and {}?", &guess, &new_item);
                        let question = Input::new()
                        .with_prompt(prompt)
                        .interact()?;
                        sleep(Duration::from_millis(200));
                        let prompt = format!("For {}, {}", &new_item, &question);
                        let boxed_new_item = Box::new(Node::Item(new_item));
                        let boxed_old_item = Box::new(this_node.clone());
                        let (yes,no) = if Confirm::with_theme(&ColorfulTheme::default())
                        .with_prompt(prompt)
                        .interact()?  {
                            (boxed_new_item, boxed_old_item)
                        } else {
                            (boxed_old_item, boxed_new_item)
                        };
                        // Transform this item-node into a question-node (containing old and new items)
                        *this_node = Node::Question(BinaryQ {question, yes, no});
                        break;
                    }
                },
            }
        }
        save_file(&domain, &db)?;
        println!("\n\nLet's play again...\n");
        sleep(Duration::from_millis(2000));
    }
}

fn read_or_initialize(domain: &mut String, db: &mut Node) -> Result<()> {
    *domain = "the kitchen".into();
    let filename = convert_to_filename(domain, ".json");
    let path = Path::new(&filename);
    
    // Open the path in read-only mode, returns `io::Result<File>`
    match File::open(&path) {
        Ok(file) => {
            let reader = BufReader::new(file);
            *db = serde_json::from_reader(reader)?;
        },
        Err(_why) => {
            *db = Node::Item("the refrigerator".into())
        },
    };

    Ok(())
}

fn save_file(domain: &String, db: &Node) -> Result<()> {
    let filename = convert_to_filename(domain, ".json");
    let path = Path::new(&filename);
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why),
        Ok(file) => file,
    };

    let writer = BufWriter::new(file);
    serde_json::to_writer_pretty(writer, db)?;

    Ok(())
}

fn convert_to_filename(arb_string: &str, postfix: &str) -> String {
    arb_string.trim().replace(" ", "_").to_string() + postfix
}