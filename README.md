# binary_questions

20-questions style guessing game that starts out stupid and learns!
Written in Rust

I came across this algorithm as a kid in the mid 70's.  Its simple power impressed me, and I've never forgotten it.
Show it to a kid in your family.

(BTW, I had to type in the code from a magazine.  I think the machine's only storage was a tape drive -- a very SLOW tape drive.)
